package com.example.empleos.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.empleos.model.Vacante;

@Service
public class VacantesServiceImpl implements IVacantesService {

	private List<Vacante> lista = null;

	public VacantesServiceImpl() {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		lista = new LinkedList<Vacante>();
		
		try {
			Vacante vacante1 = new Vacante();
			vacante1.setId(1);
			vacante1.setNombre("Ingeniero de comunicaciones");
			vacante1.setDescripcion("Se solicita ingeniero para dar soporte a intranet");
			vacante1.setFecha(sdf.parse("08-02-2019"));
			vacante1.setSalario(1000.0);
			vacante1.setDestacado(1);
			vacante1.setEstatus("Aprobada");
			vacante1.setImagen("empresa1.jpg");
			
				
			Vacante vacante2 = new Vacante();
			vacante2.setId(2);		
			vacante2.setNombre("Contador publico");
			vacante2.setDescripcion("Empresa importante solicita contador con 5 años de experiencia titulado");
			vacante2.setFecha(sdf.parse("09-02-2019"));
			vacante2.setSalario(2000.0);
			vacante2.setDestacado(0);
			vacante2.setEstatus("Aprobada");
			vacante2.setImagen("empresa2.jpg");
						
			Vacante vacante3 = new Vacante();
			vacante3.setId(3);
			vacante3.setNombre("Ingeniero eléctrico");
			vacante3.setDescripcion("Empresa internacional solicita Ingeniero mecánico para mantenimiento de la instalación eléctrica");
			vacante3.setFecha(sdf.parse("10-02-2019"));
			vacante3.setSalario(1200.0);
			vacante3.setEstatus("Eliminada");
			vacante3.setDestacado(0);
				
			Vacante vacante4 = new Vacante();
			vacante4.setId(4);
			vacante4.setNombre("Diseñador gráfico");
			vacante4.setDescripcion("Solicitamos Diseñador Gráfico titulado para diseñar publicidad de la empresa");
			vacante4.setFecha(sdf.parse("11-02-2019"));
			vacante4.setSalario(1600.0);			
			vacante4.setDestacado(1);
			vacante4.setEstatus("Creada");
			vacante4.setImagen("empresa4.jpg");
			
			lista.add(vacante1);
			lista.add(vacante2);
			lista.add(vacante3);
			lista.add(vacante4);
			
		}catch(ParseException e) {
			System.out.println("Error " + e.getMessage());
		}
		
	}
	
	public List<Vacante> getLista() {
		return lista;
	}



	public void setLista(List<Vacante> lista) {
		this.lista = lista;
	}
	
	@Override
	public List<Vacante> buscarTodas() {
	
		return lista;
	}


	@Override
	public Vacante buscarPorId(int idVacante) {
		List<Vacante> lista = getLista();
		return lista.get(idVacante-1);
	}

	@Override
	public void guardar(Vacante vacante) {
		lista.add(vacante);
		
	}

	@Override
	public List<Vacante> buscarDestacadas() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void eliminar(Integer idVacante) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Vacante> buscarByExample(Example<Vacante> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Vacante> buscarTodas(Pageable page) {
		// TODO Auto-generated method stub
		return null;
	}

}
