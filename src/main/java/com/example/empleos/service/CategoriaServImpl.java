package com.example.empleos.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.empleos.model.Categoria;

@Service
public class CategoriaServImpl implements ICategoriasService {

	private List<Categoria> lista = null;

	public CategoriaServImpl() {
		
		lista = new LinkedList<Categoria>();
		try {
			Categoria cat1 = new Categoria();
			cat1.setId(1);
			cat1.setNombre("INFORMÁTICA");
			cat1.setDescripcion("trabajos relacionados con la informática");
			
			Categoria cat2 = new Categoria();
			cat2.setId(2);
			cat2.setNombre("CONSTRUCCIÓN");
			cat2.setDescripcion("trabajos relacionados con la construcción");
			
			Categoria cat3 = new Categoria();
			cat3.setId(3);
			cat3.setNombre("VENTAS");
			cat3.setDescripcion("trabajos relacionados con las ventas");
			
			Categoria cat4 = new Categoria();
			cat4.setId(4);
			cat4.setNombre("EDUCACIÓN");
			cat4.setDescripcion("trabajos relacionados con las educación");
			
			lista.add(cat1);
			lista.add(cat2);
			lista.add(cat3);
			lista.add(cat4);
			
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	
	public List<Categoria> getLista() {
		return lista;
	}



	public void setLista(List<Categoria> lista) {
		this.lista = lista;
	}



	@Override
	public void guardar(Categoria categoria) {
		System.out.println(categoria.getId());
		categoria.setId(lista.size()+1);
		lista.add(categoria);
	}

	@Override
	public List<Categoria> buscarTodas() {		
		return getLista();
	}

	@Override
	public Categoria buscarPorId(Integer idCategoria) {
		List<Categoria> lista = getLista();
		return lista.get(idCategoria-1);
	}



	@Override
	public void eliminar(Integer idCategoria) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public Page<Categoria> buscarTodas(Pageable page) {
		// TODO Auto-generated method stub
		return null;
	}

}
