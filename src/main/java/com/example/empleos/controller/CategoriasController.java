package com.example.empleos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.empleos.model.Categoria;
import com.example.empleos.service.ICategoriasService;

@Controller
@RequestMapping(value="/categorias")
public class CategoriasController {
	
	@Autowired
	//@Qualifier("categoriasServiceJpa")
	private ICategoriasService serviceCategoria;
	
	@RequestMapping(value="/index", method=RequestMethod.GET)
	public String mostrarIndex(Model model) {

		List<Categoria> categorias = serviceCategoria.buscarTodas();
		model.addAttribute("categorias", categorias);
		
		return "categorias/listCategorias";
	}
	
	@RequestMapping(value="/indexPaginate",method=RequestMethod.GET)
	public String mostrarIndexPaginado(Model model, Pageable page) {
		Page<Categoria> lista = serviceCategoria.buscarTodas(page);
		model.addAttribute("categorias",lista);
		return "categorias/listCategorias";
	}
	
	@RequestMapping(value="create", method=RequestMethod.GET)
	public String crear(Categoria categoria, Model model) {
		model.addAttribute("categorias", serviceCategoria.buscarTodas());
		return "categorias/formCategoria";
	}
	
	@RequestMapping(value="/save", method=RequestMethod.POST)
	public String guardar(Categoria categoria, BindingResult result, RedirectAttributes attributes) {
		
		//Guardar el objeto Categoria a traves de la clase de servicio
		// *				   Validar errores de Data Binding y mostrarlos al usuario en caso de haber
		// *				   Mostrar al usuario mensaje de confirmacion de registro guardado
		// *	
		if(result.hasErrors())
		{
			for(ObjectError error: result.getAllErrors()) {
				System.out.println("Ocurrio un error: " + error.getDefaultMessage());
			}
			return "categorias/formCategoria";
		} 
		
		attributes.addFlashAttribute("msg","Registro Guardado");
		
		serviceCategoria.guardar(categoria);
		System.out.println(categoria);
		
		return "redirect:/categorias/index";
	}
	
	
	@RequestMapping(value="/edit/{id}", method = RequestMethod.GET)
	public String editar(@PathVariable("id") int idCategoria,Model model) {
		Categoria categoria = serviceCategoria.buscarPorId(idCategoria);
		System.out.println("Encontrada cat = " + categoria.getNombre());

		model.addAttribute("categoria", categoria);
		return "categorias/formCategoria";
	}
	
	
	@RequestMapping(value="/delete/{id}", method = RequestMethod.GET)
	public String eliminar(@PathVariable("id") int idCategoria,RedirectAttributes attributes) {
		
		serviceCategoria.eliminar(idCategoria);
		attributes.addFlashAttribute("msg", "La categoria fue eliminada");
		return "redirect:/categorias/indexPaginate";
		
	}
}


